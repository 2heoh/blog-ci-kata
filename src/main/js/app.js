const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');
const Entry = require('./Entry.react');

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {entries: []};
    }

    componentDidMount() {
        client({method: 'GET', path: '/entries'}).done(response => {
            this.setState({entries: response.entity});
        });
    }

    render() {
        return (
            <EntryList entries={this.state.entries}/>
        )
    }
}

class EntryList extends React.Component {
    render() {
        const entries = this.props.entries.map(entry =>
            <Entry key={entry.id} entry={entry}/>
        );
        return (
            <table width="100%">
                <tbody>
                    <tr>
                        <th>
                            <h1>Blog</h1>
                        </th>
                    </tr>
                    {entries}
                </tbody>
            </table>
        )
    }
}

ReactDOM.render(
    <App/>
    ,
    document.getElementById('react')
)