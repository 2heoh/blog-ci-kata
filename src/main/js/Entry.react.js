'use strict'
const React = require('react');

class Entry extends React.Component {
    render() {
        return (
            <tr>
                <td align="center">
                    <table>
                        <tbody>
                        <tr>
                            <td>{this.props.entry.title}</td>
                            <td><a href="#">@{this.props.entry.author}</a></td>
                        </tr>
                        <tr>
                            <td colSpan="2">
                                {this.props.entry.text.substring(0, 100)}
                            </td>
                        </tr>
                        <tr>
                            <td colSpan="2">{this.props.entry.tag}<hr/></td>
                        </tr>
                        </tbody>
                    </table>

                </td>
            </tr>
        )
    }
}

module.exports = Entry;