'use strict';

const entry = {
  title: 'title',
  author: 'author',
  text: 'text',
  tag: 'tag'
};

const React = require('react');
const Entry = require('../Entry.react');
const renderer = require('react-test-renderer');


it('renders correctly', () => {

  const tree = renderer
    .create(<Entry entry={entry} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});