package ru.agilix.blog.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Entry {
    private @Id
    @GeneratedValue
    Long id;
    private String title;
    private String text;
    private String author;

    @Override
    public String toString() {
        return "Entry{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", author='" + author + '\'' +
                ", tag='" + tag + '\'' +
                '}';
    }

    private String tag;

    public Entry() {
    }

    public Entry(String title, String text, String author, String tag) {
        this.title = title;
        this.text = text;
        this.author = author;
        this.tag = tag;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }


    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.title, this.text);
    }

}
