FROM node:12 as front

WORKDIR /src

COPY . .

RUN npm install
RUN npm test
RUN ./node_modules/webpack/bin/webpack.js -d --output ./bundle.js

FROM gradle as compiler

WORKDIR /src

COPY . .
COPY --from=front /src/bundle.js ./src/main/resources/static/built/

RUN gradle build

FROM anapsix/alpine-java:latest

COPY --from=compiler /src/build/libs/blog-ci-kata-0.0.1-SNAPSHOT.jar /

EXPOSE 8080

CMD ["java", "-jar", "/blog-ci-kata-0.0.1-SNAPSHOT.jar"]



