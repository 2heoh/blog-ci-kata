# Blog CI Kata

Simple blog engine - java Spring Boot rest api as backend, react.js as frontend

## Project build

```json
sh build-script.sh
```

## Backend manually

### Run build
```
./gradlew build
```

### Run service

Service listens http://localhost:8080/

```
./gradlew bootRun
```

## Build project manually

### Install dependencies
```
npm install
```

### Build 
```json
npm run watch
```

